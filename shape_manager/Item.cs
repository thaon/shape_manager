using System;

namespace shape_manager
{
	public class Item
	{
		public int _ID;
		public string _name;
		public ShapeInterface containedObject;

		public Item (int ID, string name, ShapeInterface c_o)
		{
			this._ID = ID;
			this._name = name;
			this.containedObject = c_o;
		}
	}
}

