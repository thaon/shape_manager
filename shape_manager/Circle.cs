using System;

namespace shape_manager
{
	public class Circle : ShapeInterface
	{
		public float _radius;

		public Circle (float radius)
		{
			this._radius = radius;
		}

		public void resize()
		{
			Console.WriteLine ("Insert the new Radius for the selected shape:");
			float newSize = float.Parse(Console.ReadLine ());
			_radius = newSize;
		}

		public float get_area ()
		{
			return (float)Math.PI * _radius * _radius;
		}
	}
}

