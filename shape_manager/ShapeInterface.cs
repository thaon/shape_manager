using System;

namespace shape_manager
{
	public interface ShapeInterface
	{
		float get_area();
		void resize();
	}
}

