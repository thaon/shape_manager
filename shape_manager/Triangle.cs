using System;

namespace shape_manager
{
	public class Triangle : ShapeInterface
	{

		public float _edge1, _edge2, _angle;

		public Triangle (float edge1, float edge2, float angle)
		{
			this._edge1 = edge1;
			this._edge2 = edge2;
			this._angle = angle;
		}

		public void resize()
		{
			Console.WriteLine ("Insert the first new edge for the selected shape:");
			float newEdge1 = float.Parse(Console.ReadLine ());
			Console.WriteLine ("Insert the second new edge for the selected shape:");
			float newEdge2 = float.Parse(Console.ReadLine ());
			Console.WriteLine ("Insert the new angle for the selected shape:");
			float newAngle = float.Parse(Console.ReadLine ());
			_edge1 = newEdge1;
			_edge2 = newEdge2;
			_angle = newAngle;
		}

		public float get_area()
		{
			return ((float)(_edge1*_edge2*Math.Sin(_angle))/2);
		}
	}
}

