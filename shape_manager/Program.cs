using System;
using System.Collections.Generic;

namespace shape_manager
{
	class MainClass
	{
		static bool end = false;
		public static Item selectedItem;
		public static int ID = 0;

		public static List<Item> itemList = new List<Item> ();

		static void Pause()
		{
			Console.WriteLine ("Press Enter to continue...");
			Console.ReadLine ();
		}

		static void show_help()
		{
			Console.WriteLine ("Available commands:\n" +
				"sq = create a new square\n" +
				"tr = create a new triangle\n" +
				"cr = create a new circle\n" +
				"del = deletes the last shape created\n" +
				"sel = asks for a shape name and selects it\n" +
				"rn = renames the selected shape\n" +
				"rs = resizes the selected shape\n" +
				"A = shows the area of the selected shape\n" +
				"N.B. if no shape is selected, the last shape created will be automatically selected\n" +
				"ls = lists the shapes created\n" +
				"quit = ends the program\n");
			Pause ();
		}

		static int execute_command()
		{
			string command = Console.ReadLine ();
			switch (command)
			{
			case "sq":
				Square sq = new Square (1);
				Item sqi = new Item (ID, "Square "+ID, sq);
				itemList.Add (sqi);
				selectedItem = sqi;
				ID++;
				return 1;

			case "tr":
				Triangle tr = new Triangle (1,1,90);
				Item tri = new Item (ID, "Triangle "+ID, tr);
				itemList.Add (tri);
				selectedItem = tri;
				ID++;
				return 2;

			case "cr":
				Circle cr = new Circle (1);
				Item cri = new Item (ID, "Circle "+ID, cr);
				itemList.Add (cri);
				selectedItem = cri;
				ID++;
				return 3;

			case "del":
				remove_item (selectedItem);
					return 4;

			case "sel":
					Console.WriteLine ("Insert the name of the shape you want to select:");
					string itemName = Console.ReadLine ();
					if (itemName != "" && itemList.Exists (x => x._name == itemName))
					{
						selectedItem = itemList.Find (x => x._name == itemName);
						Console.WriteLine (itemName + " is now selected");
						Pause ();
					}
					else
					{
						if (selectedItem != null)
						{
							Console.WriteLine("could not find a shape named " + itemName);
							Console.WriteLine ("going back to previous selection: " + selectedItem._name);
						}
						else Console.WriteLine ("Shape name must be entered to select it");
						Pause ();
					}
					return 5;

			case "rn":
					if (selectedItem != null)
					{
						Console.WriteLine ("Insert the new name for the selected shape:");
						string newName = Console.ReadLine ();
						Console.WriteLine (selectedItem._name + " renamed to " + newName);
						selectedItem._name = newName;
					}
					else Console.WriteLine ("Select a shape before attempting to rename, smartass");
					Pause ();

					return 6;

			case "rs":
					if (selectedItem != null)
					{
						selectedItem.containedObject.resize ();
						Console.WriteLine ("Shape Resized");
					}
					else Console.WriteLine ("Select a shape before resizing it!");
					Pause ();
					return 7;

			case "A":
					Console.WriteLine ("The area of the selected shape '" + selectedItem._name + "' is: " + selectedItem.containedObject.get_area ());
					Console.ReadLine ();
					return 8;

			case "ls":
					foreach (Item itm in itemList)
					{
						Console.WriteLine (itm._name);
					}
					Pause ();
					return 9;

				case "quit":
					end = true;
					return 10;

				case "help":
					show_help ();
					return 11;

				default:
					return 0;
			}
		}

		public static void remove_item(Item shape)
		{
			itemList.Remove (selectedItem);
			selectedItem = itemList.Find(x => x._ID==itemList.Count-1);
		}

		public static void Main (string[] args)
		{

			while (!end)
			{
				Console.Clear ();
				Console.WriteLine ("Welcome to shape manager, type 'help' for options!");
				execute_command ();
			}
		}
	}
}
