using System;

namespace shape_manager
{
	public class Square : ShapeInterface
	{

		public float _edge;

		public Square (float edge)
		{
			this._edge = edge;
		}

		public void resize()
		{
			Console.WriteLine ("Insert the new R for the selected shape:");
			float newSize = float.Parse(Console.ReadLine ());
			_edge = newSize;
		}

		public float get_area()
		{
			return (_edge * _edge);
		}
	}
}

